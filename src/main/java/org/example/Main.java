package org.example;

import org.example.activities.RunningActivity;
import org.example.activities.SwimmingActivity;

import java.util.Scanner;

public class Main {

    public static String APP_NAME = "Smart Fitness EU";
    public static final String APP_LABEL = "- = " + APP_NAME + "= -";
    private static final Display display = new Display();
    private static Scanner in;
    public static void main(String[] args) {
        Person user = greetingUser();
        in = autenticateUser(user);
        if (in == null)
            return;
        display.showActivityMenu();
        String userAnswer = interogateUser(in);
        ChooseActivity(userAnswer);
        display.showAdditionalActivityMenu();
        userAnswer = interogateUser(in);
        while(userAnswer.equalsIgnoreCase("yes")) {
            display.showActivityMenu();
            userAnswer = interogateUser(in);
            ChooseActivity(userAnswer);
            display.showAdditionalActivityMenu();
            userAnswer = interogateUser(in);
            if (userAnswer.equalsIgnoreCase("no")) {
                break;
            }
        }
        display.goodByeMessage();
    }
    private static String interogateUser(Scanner scanner) {
        return scanner.nextLine();
    }

    private static void ChooseActivity(String activity) {
        switch (activity) {
            case "1" : {
                SwimmingActivity swimming = new SwimmingActivity("light");
                display.printChosenActivity(swimming);
                display.printMessage(swimming.askAboutNoOfPools());
                String noOfPools = interogateUser(in);
                int nrOfPools = Integer.parseInt(noOfPools);
                swimming.withNoOfPools(nrOfPools);
                display.printMessage(swimming.provideSwimmStatistics());
                break;
            }
            case "2" : {
                RunningActivity running = new RunningActivity("light");
                display.printChosenActivity(running);
                display.printMessage(running.askAboutNumberOfKm());
                String userInput = interogateUser(in);
                int noOfKm = Integer.parseInt(userInput);
                running.withNoOfKm(noOfKm);
                display.printMessage(running.provideStatistics());
                break;
            }
            case "3" : {
                System.out.println("You have choosen dancing from the Fitness Menu");
                break;
            }
            case "4" :  {
                System.out.println("1.Outdoor Cycling");
                System.out.println("2.Indoor Cycling");
                System.out.println("3.Spinning Cycling");
                System.out.println("4.Soul Cycling");
                break;
            }
            case "5" : {
                System.out.println("You have choosen fitness from the Fitness Menu");
                break;
            }
            case "6" : {
                System.out.println("You have choosen yoga from the Fitness Menu");
                break;
            }
        }
    }

    private static Scanner autenticateUser(Person user) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter your Pin number:");
        String pinCode = interogateUser(scanner);
        if (!user.getPinCode().equals(pinCode)) {
            System.out.println("wrong pin...");
            return null;
        }
        System.out.println("Successfully logged in. ");
        return scanner;
    }

    private static Person greetingUser() {
        display.printAppName();
        Scanner scanner = new Scanner(System.in);
        display.askUserForName();
        Person user = new Person("John", "Other", "m", "2860604124425", "9999", "5444");
        String userAnswer = interogateUser(scanner);
        display.greatKnownUser(user);
        return user;

    }

}



// We want to build a Fitness Application.
// what is the name of the app? -> Smart Fitness EU
// What does a Fitness application do ?
// We need a plan !
// Summary of your activities -> we need an activity.
// Numbers of steps completed in a given timeframe.
// Monitor the heart rate of the person.
// Stairs climbed.
// Persons details like age / weight / height.