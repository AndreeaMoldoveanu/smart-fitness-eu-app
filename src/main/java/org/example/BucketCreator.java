package org.example;

public class BucketCreator {
    public static void main(String[] args) {
        Bucket roundCastle = new Bucket("round", 1, "mini tours");
        Bucket squareCastle = new Bucket("square", 12, "simple tours");
        Bucket roundCastleWithSizeOfFive = new Bucket(roundCastle, 5);

        roundCastle.whatIsYourSize();
        roundCastle.whatIsYourShape();
        roundCastle.whatAreYou();

        squareCastle.whatIsYourSize();
        squareCastle.whatIsYourShape();
        squareCastle.whatAreYou();

        roundCastleWithSizeOfFive.whatAreYou();
    }
}
