package org.example.activities;

import org.example.activities.Activity;

public class RunningActivity extends Activity {

    private static final String RUNNING = "Running";

    private final static int BURNED_CALORIES_PER_KM = 150;
    private final static int STEPS_PER_KM = 1450;
    private int noOfKmRun = 0;


    public RunningActivity(String level) {
        super(level, RUNNING);
    }

    public String askAboutNumberOfKm() {
        return "How many km did you run today?";
    }

    public void withNoOfKm(int noOfKm) {
        this.noOfKmRun = noOfKm;
    }

    public String provideStatistics() {
        return "You have run " + noOfKmRun + "km, " + "burned " + calculateBurnedCalories() + " calories, and made " + calculateTotalSteps() + " steps" ;
    }

    public int calculateBurnedCalories() {
        int calories = this.noOfKmRun * BURNED_CALORIES_PER_KM;
        return calories;
    }
    public int calculateTotalSteps (){
        int steps = this.noOfKmRun * STEPS_PER_KM;
        return steps;
    }
}
