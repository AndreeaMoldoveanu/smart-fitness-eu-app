package org.example.activities;

import org.example.activities.Activity;

public class SwimmingActivity extends Activity {

    private static final String SWIMMING = "swimming";
    private static final int POOL_LENGHT = 50;

    private static final int BURRNED_CALORIES_PER_POOL = 200;
    public int noOfPools = 0;

    public SwimmingActivity(String level) {
        super(level, SWIMMING);
    }

    public void withNoOfPools(int noOfPools) {
        this.noOfPools = noOfPools;
    }

    public int getBurnedCalories() {
        int Calories = BURRNED_CALORIES_PER_POOL * this.noOfPools;
        return Calories;
    }

    public int getSwimmingDistance() {
        int distance = POOL_LENGHT * this.noOfPools;
        return distance;
    }
    public String askAboutNoOfPools() {
        return "How many swimming pools did you complete today?";
    }
    public String provideSwimmStatistics() {
        return "You have burned " + getBurnedCalories() + " in " + getSwimmingDistance() + "meters swim.";
    }
}
