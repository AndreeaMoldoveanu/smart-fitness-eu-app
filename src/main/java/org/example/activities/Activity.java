package org.example.activities;

public class Activity {
    private final String intensity;

    private final String name;

    public Activity(String level, String name) {
        this.intensity = level;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getIntensity() {
        return intensity;
    }

}
