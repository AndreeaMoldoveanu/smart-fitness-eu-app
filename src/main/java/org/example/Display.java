package org.example;

import org.example.activities.Activity;

import static org.example.Main.*;

public class Display {

    private static final int NO_OF_STEPS = 12345;

    public void printAppName() {
        System.out.println(APP_LABEL);
    }
   public void askUserForName() {
        System.out.println("What is your name ?");
    }
    public void greatKnownUser(Person user) {
        System.out.println("Welcome to the "  + APP_NAME +" " + user.getname + " !");
    }

    public void showActivityMenu() {
        //System.out.println("Congrats! You have reached your " + NO_OF_STEPS + " steps.");
        System.out.println("What would you like to do today?");
        System.out.println("1.Swimming");
        System.out.println("2.Running");
        System.out.println("3.Dancing");
        System.out.println("4.Cycling");
        System.out.println("5.Fitness");
        System.out.println("6.Yoga");

    }
    public void showAdditionalActivityMenu() {
        System.out.println("Would you like to add additional activities?");
        System.out.println("Yes");
        System.out.println("No");
    }
    public void printChosenActivity(Activity activity) {
        System.out.println("You have choosen "+ activity.getName() + " from the Fitness Menu");
    }
    public void printMessage(String msg) {
        System.out.println(msg);
    }
    public void goodByeMessage() {
        System.out.println("Bye, come back tomorow.");
    }
}
