package org.example;

public class Bucket {
    private final String shape;
    private final int size;
    private final String decorations;

    public Bucket(String shape, int size, String decorations) {
        this.shape = shape;
        this.size = size;
        this.decorations = decorations;
    }

    public Bucket(Bucket castle, int size) {
        this.shape = castle.shape;
        this.size = castle.size;
        this.decorations = castle.decorations;
    }

    public void whatAreYou() {
        System.out.println("I am a " + shape + " Castle of size " + size + " with intersting " + decorations);
    }
    public void whatIsYourSize() {
        System.out.println("My size is " +  size);
    }
    public void whatIsYourShape() {
        System.out.println("My shape is: " + shape);
    }
}
